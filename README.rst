kinetic Activation-Relaxation Technique Read the Docs documentation
===================================================================

GitLab for kinetic Activation-Relaxation Technique (kART) Sphinx docs.

Clike here  to access the documentation page: https://kart-doc.readthedocs.io/en/latest/

Build
-----

Clone kART-doc project.

.. code::

    git clone https://gitlab.com/groupe_mousseau/kart-doc.git
    cd kart-doc

Set build environment.

.. code::

    virtualenv venv
    source venv/bin/activate
    python -m pip install --upgrade pip
    pip install -r docs/requirements.txt

Build.

.. code::

    sphinx-build -b html docs/source/ _build/html

Run.

.. code::

    firefox _build/html/index.html




