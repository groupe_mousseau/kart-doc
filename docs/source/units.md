# Units

Units are, in principle, defined by the forcefield. k-ART, per se, is
takes the forces and updates the positions accordingly.

However, most parameters provided initially for convergence to saddle
points and minimization are optimized for energies in eV and
displacements in Å. It is therefore simpler, but not essential, to use
these units. At the moment, it only takes `kcal2ev`, but it is
straightforward to expand this.

A command is provided to convert energy units to eV :

      # Converts the energy units provided by the force code into a desired value for ART
      setenv UNITS_CONVERSION      metal
