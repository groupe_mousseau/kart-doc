# Download the source code

> Important: the kART code is undergoing some important changes. The current version uses a version of ART that is integrated into lammps (pART), this makes it faster, but looses some of the capabilities of the original code (local force calculations, for example). Moreover, this new version  does not match fully the documentation here.
>
> **It is therefore recommended, if you are using kART for the first time, to download the previous version of the code, which handles ARTn by itself.**

## Current version of the code

The source code is now available through https://gitlab.com at https://gitlab.com/groupe_mousseau/kart. To obtain
the code, please create an account and contact Normand Mousseau or
another administrator to gain access.

If you want to create a local git repo named `kart`, follow these steps:

      git clone https://gitlab.com/groupe_mousseau/kart.git kart
            *  Enter your gitlab username when asked
            *  Enter your gitlab password when asked
      cd kart; git log --oneline --decorate --name-status

The first line of the output of step 3 (test) should look like this:

      8c880bf (HEAD -> master, origin/master, origin/HEAD) Add saddle_push object in dependencies

k-ART is to be used with the `LAMMPS` library and `artn-plugin`.

More information on https://github.com/lammps/lammps and https://gitlab.com/mammasmias/artn-plugin.


## Previous version (that corresponds ot the documentation and has its own ARTn version)

> This version is recommended for first-time users

Download kART as previously discussed

  git clone https://gitlab.com/groupe_mousseau/kart.git kart
    cd kart

But once downloaded, get back the right version:

    git checkout 62d66adf

