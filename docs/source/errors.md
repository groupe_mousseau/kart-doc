# Handling errors

The code can stop for several reasons. Here some of the most common
errors messages and how to solve them:

**Message**: "*no message*\", the code stops immediately or hanged (no
k-ART calculations). This is mainly due to a bad configuration of the
lammps parameters. Check the "*in.lammps*\" file: right path to
potential file, etc; you should use log.lammps instruction to print the
lammps output (be careful this produces a huge file. You might not want
to use it after debugging).

**Message**: "*the tree is empty: this should never happen*\". This
message means that no events are found for any topology of the current
state, in other words no saddles have been found for the existing
topologies. Several issues may cause this to happen, all are related to
bad optimization parameters. Then check if system is able to leave the
basin or check the number of iterations used by *kter* and *iter*, or if
`INCREMENT_SIZE` is too large, or etc (see section
 [12](#sec:Tuning the parameters for a particular system){reference-type="ref"
reference="sec:Tuning the parameters for a particular system"}).

[ \....STILL REMAINS TO EXPLAIN MESSAGES BELLOW
\....]{style="color: red"}

**Message**: "*inv_atm_lbl = 0!!!!!'* \" from file and line:
analyze_events.f90-594-

**Message**: "*master_atm_lb/l = 0!!!!!'* \" from file and line:
analyze_events.f90-1068-

**Message**: "*ERROR: master_topology pointer for atom ', atm_lbl,' is
not associated.'* \" from file and line: analyze_events.f90-1099-

**Message**: "*master_atm_lbl = 0!!!!!'* \" from file and line:
analyze_events.f90-1392-

**Message**: "*there is a problem rehashing the generic event event
though it is new'* \" from file and line: analyze_events.f90-1768-

**Message**: "*we will stop now!!!!!'* \" from file and line:
analyze_events.f90:2008:

**Message**: "*Use old update_topos'* \" from file and line:
basin_mrm.f90-508-

**Message**: "*Error opening basin graph file',BASIN_GRAPH* \" from file
and line: basin_mrm.f90-543-

**Message**: "*old:', old_topoid, ' new:', topoid* \" from file and
line: basin_mrm.f90-2010-

**Message**: "*choose ALL or VER'* \" from file and line:
Bazant.f90-724-

**Message**: "*Error:: I am Blocked - Tree is damaged'* \" from file and
line: Btree.f90-96-

**Message**: " *there should a PROBLEM event is not supposed to be in
tree'* \" from file and line: Btree.f90-174-

**Message**: "*choose ALL or VER'* \" from file and line:
calcfo_Fe.f90-244-

**Message**: "*choose ALL or VER'* \" from file and line:
calcfo_Fe.f90-878-

**Message**: "*ERROR: Cannot create local lmp pointer if lammps is
parallelised'* \" from file and line: calcfo_lammps.f90-101-

**Message**: "*pos is not allocated for rank : ', rank* \" from file and
line: calcfo_lammps.f90-274-

**Message**: "*num: ', num, ' NUMBER_ATOMS: ', NUMBER_ATOMS* \" from
file and line: calcfo_lammps.f90-295-

**Message**: "*pos is not allocated for rank : ', rank* \" from file and
line: calcfo_lammps.f90-391-

**Message**: "*num: ', num, ' nat_local:', nat_local* \" from file and
line: calcfo_lammps.f90-422-

**Message**: "*Your choice of potential is not set up for local force
calculations, please change this choice and restart'*\" from file and
line: calcforce.f90-127-

**Message**: "*The potential radius is bigger than the topo radius,
program will stop'* \" from file and line: calcforce.f90:186:

**Message**: "*Your choice of potential is not set up for local force
calculations, please change this choice and restart'*\" from file and
line: calcforce.f90-217-

**Message**: "*choose ALL or VER'* \" from file and line:
calcfo_si.f90-129-

**Message**: "*choose ALL or VER'* \" from file and line:
calcfo_si.f90-336-

**Message**: "*choose ALL or VER'* \" from file and line:
calcfo_si.f90-549-

**Message**: "*the program should stop'* \" from file and line:
calcfo_sih.f90:728:

**Message**: "*be careful the tail is not specified '* \" from file and
line: class_Btree.f90-270-

**Message**: " *error !!! node cannot be inserted, exiting the tree '*
\" from file and line: class_Btree.f90-293-

**Message**: "*current barrier higher than basin, we* \" from file and
line: event_histo.f90-256-

**Message**: "*But found a total of ', count, 'atoms with that
topology.'* \" from file and line: event_histo.f90-330-

**Message**: "*we moved to state', cur_state(cur_basin), ', but were
unsucessful.'* \" from file and line: execute_event.f90-162-

**Message**: "*we moved to state', cur_state(cur_basin), ', but were
unsucessful.'* \" from file and line: execute_event.f90-255-

**Message**: "*we moved to state', cur_state(cur_basin), ',but were
unsucessful.'* \" from file and line: execute_event.f90-416-

**Message**: "*Moving back to state', basin_orig* \" from file and line:
execute_event.f90-418-

**Message**: "*Local basin revert is not implemented at this time'* \"
from file and line: execute_event.f90-427-

**Message**: "*we moved to state', cur_state(cur_basin), ', but were
unsucessful.'* \" from file and line: execute_event.f90-450-

**Message**: "*ERROR!, event initial topo not in present configuration'*
\" from file and line: findEvents.f90-185-

**Message**: "*ERROR! eventId has an illegal value', eventId* \" from
file and line: findEvents.f90-225-

**Message**: "*ERROR: Trying to send a refine or clone job with the
non-clone interface'* \" from file and line: findEvents.f90-1059-

**Message**: "*Trying to send a non-refine task through the clone
interface'* \" from file and line: findEvents.f90-1085-

**Message**: "*ERROR!, event's initial topo not found in current
configuration'* \" from file and line: findEvents.f90-1283-

**Message**: "*ERROR! eventId has illegal value', eventId* \" from file
and line: findEvents.f90-1320-

**Message**: " *Illegal MPI communication tag' ,tag* \" from file and
line: findEvents.f90-1839-

**Message**: "*Impossible to read the file ', RESTART_CONF, ' does not
exist'* \" from file and line: initialize_KMC.f90-329-

**Message**: "*Impossible to read the file ', INI_FILE_NAME, ' does not
exist'* \" from file and line: initialize_KMC.f90-338-

**Message**: "*Cannot restart, directory ', trim(EVENTS_DIR),' not
found'* \" from file and line: initialize_KMC.f90-425-

**Message**: "*Error: atomic types must be between 1 and 6'* \" from
file and line: initialize_KMC.f90-728-

**Message**: "*Argument energy not given '* \" from file and line:
initialize_KMC.f90-933-

**Message**: "*File ', TMPFILENAME, 'not found '* \" from file and line:
initialize_KMC.f90-937-

**Message**: "*File ', TMPFILENAME, 'not found ', 'and argument energy
not given '* \" from file and line: initialize_KMC.f90-941-

**Message**: "*Problems removing ',trim(filename),', stopping'* \" from
file and line: initialize_KMC.f90:1244:

**Message**: "*The program will end now'* \" from file and line:
initialize_KMC.f90-1482-

**Message**: "*Linked sec_topo', readid,' to ', primary_topo_id* \" from
file and line: initialize_KMC.f90-1696-

**Message**: "*count=', count* \" from file and line:
List_manip.f90-364-

**Message**: "*check sec topos again', Mtopoid, StopoId* \" from file
and line: List_manip.f90:1333:

**Message**: "*Stop file ',STOP_FILE,' found, not starting simulation'*
\" from file and line: main_KART.f90:134:

**Message**: "*Remove stop file before restarting.'* \" from file and
line: main_KART.f90:135:

**Message**: "*The program was not able to fully minimize the
configuration'* \" from file and line: main_KART.f90-241-

**Message**: " *exiting'* \" from file and line: main_KART.f90-388-

**Message**: "*event forbiden should not be in tree'* \" from file and
line: main_KART.f90-456-

**Message**: "*Stop file ',STOP_FILE,' found, exiting'* \" from file and
line: main_KART.f90:669:

**Message**: "*Stop file ',STOP_FILE,' found, exiting'* \" from file and
line: main_KART.f90:672:

**Message**: "*Stop file', STOP_FILE, ' found'* \" from file and line:
main_KART.f90:675:

**Message**: "*Will stop when basin is exited'* \" from file and line:
main_KART.f90:676:

**Message**: "*Also sendind termination signal to lammps worker
according to is ARTn master', taskindex* \" from file and line:
main_KART.f90-744-

**Message**: "*should be a problem with this topology' ,
this_topo%topo_label* \" from file and line: mapping_module.f90-139-

**Message**: "*cannot proceed: choose fin or sad '* \" from file and
line: mapping_module.f90-262-

**Message**: "*exiting '* \" from file and line: mapping_module.f90-680-

**Message**: "*cannot proceed: choose fin or sad '* \" from file and
line: mapping_module.f90-756-

**Message**: "*Correspondance from event to refconfig cluster not OK'*
\" from file and line: mapping_module.f90-922-

**Message**: "*ERROR: secondary topology should not be splitted'* \"
from file and line: mapping_operations.f90-59-

**Message**: "*Giving up'* \" from file and line:
mapping_operations.f90-81-

**Message**: "*Giving up'* \" from file and line:
mapping_operations.f90-141-

**Message**: "*Giving up'* \" from file and line:
mapping_operations.f90-183-

**Message**: "*Not able to further split that topology. Giving up.'* \"
from file and line: mapping_operations.f90-323-

**Message**: "*this_topo%topo_label is different to topoId'* \" from
file and line: mapping_operations.f90-572-

**Message**: "*topoId send is null exit the routine'* \" from file and
line: mapping_operations.f90-580-

**Message**: "*REMOVING secondary topo:',tmp_topo%topo_label ,'for
primary topo :', this_topo%topo_label* \" from file and line:
mapping_operations.f90-623-

**Message**: " *choose: SW, SWA, EDP, EAM, LAM \.... '* \" from file and
line: read_param.f90-44-

**Message**: "*ERROR, INPUT_LAMMPS_FILE missing and default 'in.lammps'
not present.'* \" from file and line: read_param.f90-55-

**Message**: "*ERROR, INPUT_LAMMPS_FILE with name ', dummy,' is
missing.'* \" from file and line: read_param.f90-65-

**Message**: "*Error: number of atoms is not defined : NUMBER_ATOMS'* \"
from file and line: read_param.f90-91-

**Message**: "*The number of elements in 'ATOMIC_SYMBOLS' is different
to NTYPES. The program will stop.'* \" from file and line:
read_param.f90:113:

**Message**: "*Error: please define SIMULATION_BOX or X_BOX, Y_BOX,
Z_BOX in Ang'* \" from file and line: read_param.f90-137-

**Message**: "*Error: please define TOPO_RADIUS in Ang'* \" from file
and line: read_param.f90-169-

**Message**: "*Error: please define MAX_TOPO_CUTOFF in Ang '* \" from
file and line: read_param.f90-210-

**Message**: "*Error: variable TEMPERATURE is not defined (Kelvin)'* \"
from file and line: read_param.f90-342-

**Message**: "*ERROR: MAX_SPEC_EV_NBRE must be a non-null positive
integer'* \" from file and line: read_param.f90-643-

**Message**: "*Must be between 0 and 1'* \" from file and line:
read_param.f90-776-

**Message**: " *choose: TTL, PAR '* \" from file and line:
read_param.f90-799-

**Message**: "*Error: please define LATTICE_CONSTANT '* \" from file and
line: read_param.f90-821-

**Message**: "*or use none as argument'* \" from file and line:
read_param.f90-852-

**Message**: "*Error: OSCILL_TREAT=BMRM requires
TYPE_EVENT_UPDATE=SPEC'* \" from file and line: read_param.f90-1020-

**Message**: "*Error : only global or local type of events are accepted
- provided: ', TYPE_OF_EVENTS* \" from file and line:
read_param.f90-1194-

**Message**: "*Error: RADIUS_INITIAL_DEFORMATION must be defined when
TYPE_OF_EVENTS is local'* \" from file and line: read_param.f90-1204-

**Message**: "*Error : No eigenvalue threshold provided
(EIGENVALUE_THRESHOLD)'* \" from file and line: read_param.f90-1266-

**Message**: "*Eigenvalue increasing - stop activation'* \" from file
and line: saddle_converge.f90:94:

**Message**: "*Eigenvalue increasing - stop activation'* \" from file
and line: saddle_converge.f90:95:

**Message**: "*ERROR:, atom n_cluster is zero', atom_label,
All_atoms(atom_label)%n_cluster* \" from file and line:
set_topos.f90-100-

**Message**: "*Error, topo', this_topo%topo_label,' has multiplicity of
0'* \" from file and line: utils.f90-396-

**Message**: "*Error, topo', this_topo%topo_label,' has no particles in
list'* \" from file and line: utils.f90-418-

**Message**: "*ARTn_ranksize is not a integer, we will stop now.'* \"
from file and line: Vars.f90:545:

**Message**: "*ARTn_ranksize not equal to rank_size, we will stop'* \"
from file and line: Vars.f90:558:

**Message**: "*there is a error in the type of double parallelisation '*
\" from file and line: Vars.f90-759-

**Message**: "*HINT: Make sure you set the variable NTRAVAILLEUR in the
KMC.sh, and that the number of core',* \" from file and line:
Vars.f90-761-

**Message**: " *select a valid flag for UPDATE_VER_NEIB TTL or PAR '* \"
from file and line: Verlet_neighbour.f90-449-

**Message**:

**Message**:

**Message**:

**Message**:
