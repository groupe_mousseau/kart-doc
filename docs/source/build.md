
# Compiling the code

To load compilers module on Compute Canada, use the following commands:

    module load intel # for the Intel compilers or for GNU compilers
    module load gcc # for the GNU compilers

To load the MPI module on Compute Canada, use the following command:

    module load openmpi

To load different Blas implementation module on Compute Canada, use the following command:

    module load flexiblas # for the flexiblas lib implementation or for openblas
    module load openblas # for the openblas lib implementation or for imkl
    module load imkl # for the imkl lib implementation

## Build kART with CMake (for the latest version, using pART)

> To compile with the previous verson of kART (without ARTn integrated into kART), please go to the next section.

To build with CMake on Compute Canada, the following modules is required:

    module load cmake

If you are not the same system, ensure that CMake is available.

Make compilation will fetch its main dependency such as `lammps`, `artn-plugin`. CMake should find the available option from the environement variable (such as MPI, compilers and Blas implementation) and set them on for compilation automaticaly.

This is done in tree steps.
First Git clone kART. Then configure the cmake compilation project. Finally build kART.

    git clone https://gitlab.com/groupe_mousseau/kart.git kart
    cd kart && mkdir build && cd build                                # create and go to the build directory
    cmake ../                                                         # configuration
    cmake --build . --target main_kART                                # compilation kART

To set compilers do one or the other at the configuration setp:

    cmake ../ -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_Fortran_COMPILER=gfortran # for GNU compilers
    cmake ../ -DCMAKE_C_COMPILER=icx -DCMAKE_CXX_COMPILER=icpx -DCMAKE_Fortran_COMPILER=ifx # for IntelLLVM compilers

Setting the compiler is not mandatory, the default compiler selected on Compute Canada is the GNU Compiler Collection (GCC). Note: the compiler that compiled MPI and the compiler to build kART must be of the same family (e.g. it will not work if MPI is build with Intel and the selected compiler is GNU). This create an Unexpected EOF error on Compute Canada, when the intel module is loaded (this also load an openmpi implementation that is also build with Intel compiler) while the GNU Compuler is selected by cmake. The solution is to set the IntelLLVM compilers as shown above.


The configuration step can be modified to customize dependency of the project by amending the command with options (Syntax: `-DVARNAME=val` or `-DVARNAME val` or `-D VARNAME=val`, for bollean value `-DVARBOOL=yes/no/ON/OFF`). Other option can be set such as kART project options, lammps package (e.g. `-DPKG_MEAM=ON`), quantum espresso, compiler options and much more. See cmake documentation https://cmake.org/cmake/help/latest/index.html. Compilation speed can be increase by setting the number of core assign to compilation with -jN where N is the number of code (e.g. `cmake --build . --target main_kART -j16`)

An example of how to add lammps package (e.g. MANYBODY) to kART the configuration step is

    cmake ../ -DPKG_MANYBODY=ON

To set the Blas lib implementation to flexiblas:

    cmake ../ -DBLA_VENDOR=flexiblas

Final example where are set the compilers as IntelLLVM, the Blas lib implementation to flexiblas and the MANYBODY lammps package:

    cmake ../ -DCMAKE_C_COMPILER=icx -DCMAKE_CXX_COMPILER=icpx -DCMAKE_Fortran_COMPILER=ifx -DBLA_VENDOR=flexiblas -DPKG_MANYBODY=ON

See relevant lammps documentation at `https://docs.lammps.org/Build_basics.html`, `https://docs.lammps.org/Build_settings.html`, `https://docs.lammps.org/Build_package.html` and `https://docs.lammps.org/Build_extras.html`.

To build kART with machine-learning on-the-fly capabilities the configuration step is

    cmake ../ -DWITH_MLIP=ON

This will fetch `mlip` and `quantum espresso` automatically. Then the project is build as usual with `cmake --build . --target main_kART`.

## Build kART with Make (for the previous version, with integrated ARTn)

For now make compilation doesn't work anymore due to important change in the project.  But it is possible tu checkout to an older verssion of kart to compile it with make (this version doesn't work with `artn-plugin`). 


    git clone https://gitlab.com/groupe_mousseau/kart.git kart
    cd kart
    git checkout 62d66adf

<!-- # Testing rapidly with Docker (not working/tested)

Docker (https://docs.docker.com/compose/install/) is an environment,
available for Windows, Mac and Linux that allows sandboxing
applications. Christopher Ostrouchov porter k-ART to docker, greatly
facilitating first tests of k-kART for new users.

After you install `docker` on your computer, execute the following
commands to download an image of k-ART:

      % docker login registry.gitlab.com. [You will need to provide username and password]
      % docker pull registry.gitlab.com/groupe_mousseau/kart:master

You can now see the image in the list of docker images:

      % docker images

To run k-ART, for instance on the -vac-lammps@ example:

      % docker run -it registry.gitlab.com/groupe_mousseau/kart:master
      % cd EXAMPLES/Si-vac-lammps
      % mpirun -np 4 ./KMC.sh

Here again, more information on how to use `docker` is available in the
`wiki` page of the k-ART application on `https://gitlab.com`. -->

<!-- Outaded stuff  -->

<!-- #####   Load all the packages from LAMMPS that you need before.
For example, you might want to load the `MANYBODY` and the `MEAM` Section_packages

##### Define the nature of the library used.

By defaults, lammps build a static library. It is often preferable to used a shared library. If you include the `MPI` option, then the cmake will be compile with :

    # Build the options
    cmake ../cmake -D BUILD_MPI=yes -D BUILD_SHARED_LIBS=yes -D PKG_MANYBODY=yes -D PKG_MEAM=yes
    cmake --build .     # compilation

<!-- Since `docker` is not a very fast environment, you will need to compile
the code to run real applications. The basic package contains code in
fortran 95 and C (for the `NAUTY` part). It compiles with standard
compilers such as gnu (gfortan and C/C++) and Intel. It simply requires
the `lapack` and `blas` libraries.


There are a number of makefile.arch definitions provided in the MAKE
folder. A few particularly useful are

Linux/Intel :

:   `make intel` (with MPI) (not tested recently)

IBM/Linux/Intel (UdeM):

:   `make briaree` (with MPI) (last time tested 23 June 2015)

IBM/Linux/Lammps (UdeM) :

:   `make briareelammps` (with MPI) (last time tested 23 June 2015)

IBM/Linux/Intel (McGill) :

:   `make guillimin` (with MPI) (last time tested 23 June 2015)

IBM/Linux//Lammps (McGill) :

:   `make guilliminlammps` (with MPI) (last time tested 23 June 2015)

Mac/Intel :

:   `make mac` (serial) (not tested recently)

Mac/Gnu :

:   `make mac_gfort` (serial) (not tested recently)

Mac/MPI :

:   `make mac_mpi` (with MPI and intel) (last time tested 13
    August 2014)

Mac/Lammps/MPI:

:   `make maclammps_mpi` (with MPI,intel and lammps) (last time tested
    13 August 2014)

If they do not work, try adjusting the libraries. -->

### Building on mac OS X with Lammps and Openmpi

To build kART on a mac, with the lammps library, users can look into `Makefile.normand`. 

<!-- ### Openmpi installation

First of all, for more details, we recommend to have a look at this two
webpages :

<http://www.open-mpi.org/faq/?category=building> section 15\
<https://software.intel.com/en-us/articles/performance-tools-for-software-developers-building-open-mpi-with-the-intel-compilers#10>

Download the openmpi version that you desire on their webpage. Untar,
unzip the file, (the command should be *tar -xf
openmpi-\*.\*.\*.tar.gz*). Once it is done, go into the folder
openmpi-1.8.1 and then type the following command :\
*./configure --prefix= (Installation location) CC=icc CXX=icpc F77=ifort
FC=ifort*.

We highly recommend that you create a specific and unique path for the
installation of openmpi before typing the configure command because your
machine may already have a version of it (for example
*/usr/MPI/intel/openmpi/number_version/*) and interference may occur
between installation.

After the configuration command and if everything went well just type
*make all install*. The two previous command produce a lot of scene
output and can take about 20 mins to be done, be patient !

Now the installation is done but it is not over yet, you have to set the
PATH for the bin directory and the library directory this way :\
*export PATH=(installation_directory)/bin:\$PATH*\
*export
DYLD_LYBRARY_PATH=(installation_directory)/lib:\$DYLD_LYBRARY_PATH*

You can add this two lines in your $\sim/.profile$ or $\sim/.bashrc$ to
set them as a default path. -->

<!-- ### FFTW installation

Download the FFTW version that you desire on their webpage. Untar, unzip
the file. Once it's done go to the folder fftw and then type the
following command :\
*./configure --prefix= (Intallation location)*. Once again we highly
recommend that you create a specific installation directory (for exemple
*/usr/FFTW/*). After the configuration type *make* and then *make
install*.

Now the installation is done but it is not over yet, you have to set the
PATH for the bin directory and the library directory this way :\
*export PATH=(installation_directory)/bin:\$PATH*\
*export
DYLD_LYBRARY_PATH=(installation_directory)/lib:\$DYLD_LYBRARY_PATH* -->

<!-- ### kART and Lammps Makefiles

For LAMMPS, make sure that you compile all the libraries and load the
needed packages. The architecture that you might want to use is
*mac_mpi* but you also need to modify it by setting the path for the mpi
and fftw that you have just created.

For kART, the architecture `maclammps_mpi` is already setup. However,
you will need to verify the `bin`, `include` and `lib` directory for
these softwares : `openmpi`, `fftw`, `mkl` and `Lammps`.

One more warning: when building on Mac OSX using intel compiler, one
will probably need to source the file `mklvars.sh` with the option
intel64 (especially if the compilation succeeds but not the execution of
the code). -->

### Make  using with LAMMPS

k-ART can be compiled with the LAMMPS library, giving it access to a
wide range of efficiently programmed forcefields. k-ART includes a
modified version of the fortran interface to the library as the original
one has some limitations.

For this, download the latest version of LAMMPS :
<http://lammps.sandia.gov>

LAMMPS can be compiled with `cmake` or `make`. We provide below a brief list of commands. Please refer to LAMMPS' manual for all details. -->

#### Make version

1.  Build LAMMPS as a Library,

    a)  Load all the packages from LAMMPS that you need before
        compilation. (For example, the package
        `manybody` is needed for using tersoff and
        sw potential.)\
        `% make yes-manybody`\
        You can find the list of all the available
        packages by typing *make package-status* in the src directory of
        LAMMPS or check the list on their web site
        (<http://lammps.sandia.gov/doc/Section_packages.html>)

    b)  Now Compile LAMMPS as a library by entering :\
        `% make makelib`\
        `% make -f Makefile.lib foo` for old version of LAMMPS\
        `% make mode=shlib foo` for newest version of LAMMPS\
        (where foo is the architecture makefile name, for further
        details check the section 2.5 of their web page,
        <http://lammps.sandia.gov/doc/Section_start.html#start_5>)

        Note for "shlib" is for a shared library, for static one, just type "mode=lib"

    c)  You should have now a file named *liblammps_foo.a* in the src
        folder. You can move it to the source code directory of kART or
        update the path to the library in the k-ART Makefile.

2.  To compile k-ART with the lammps library, you need to use the
    `-DLAMMPS_VERSION` flag and set the path to the src folder of your
    lammps distribution (see the *briareelammps* option as an example). -->



<!-- ## Building on briaree (IBM/Linux/Intel)

Connection to the `briaree` server is very straightforward using the SSH
protocol. The following information may be useful. You can define this
information into a file called `config` into the `.ssh` directory that
is automatically created in your home directory:

      Host briaree
         Hostname briaree.calculquebec.ca
         Port 22

You can then login with:

`% ssh briaree`

One should have an account on compute Canada to be allowed to use the
resources of `briaree`. For further information, please visit
<https://www.computecanada.ca>.-->



To build k-ART, without the lammps
library, just go in the SRC folder and select the appropriate Makefile (found in `src/MAKEFILE`). For example `make briaree` should
create the file `KMCART_correctmap_briaree` and move it to your binary
directory (default is bin in the home directory, can be changed with the
BINDIR variable in the Makefile).

To compile k-ART with the *lammps* library, you first need to create the
*lammps* library file. Follow the instructions below to create it and
then use the LAMMPSPATH variable in the makefile under the briareelammps
flag to define its path. You can then compile by typing the command, for example:

`% make briareelammps`.

1.  If working on a cluster, load all the necessary modules used during
    compilation (intel-compilers for example) and the MPI modules if the
    parallel version is being compiled (recommended).

2.  Make the necessary modifications to the Makefile.

3.  For example, on unix (briaree), we are using the following modules
    :\
    - *intel-compilers/12.0.4.191*\
    - *MPI/Intel/openmpi/1.6.2*\
    - And if lammps version *FFTW/3.3*

<!-- ## Building on guillimin (IBM/Linux/intel)

As in the preceding subsection, connection on guillimin is made via
these informations :

      Host briaree
         Hostname guillimin.hpc.mcgill.ca
         Port 22

and login with:

`% ssh guillimin`

To build k-ART on the guillimin cluster (McGill), with or without the
*lammps* library, we refer to the same procedure as in subsection
[3.3] briaree{reference-type="ref" reference="briaree"}. One just
needs to substitute the keyword briaree for the keyword guillimin..
Also, one needs to load the appropriate modules used in the guillimin
cluster.

As an example, we are using the following modules :\
- *ifort_icc/15.0*\
- *openmpi/1.8.3-intel*\
- *FFTW/3.3-openmpi-intel*\
- *MKL/11.2*. 
