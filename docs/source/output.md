# Output

There are a number of files created during the execution of k-ART.
Details on the content of each file is given at the end of this manual.
Here is a simple description.

1.  provides a global summary of all simulation details, including
    parameters, code version, the date of the compilation and a number
    of used in the simulation as well as the basic simulation time as a
    function of event.

2.  `Sortieproc.xx` files are the log files for each core running the
    simulation (labelled from 0 to N-1, where N is the number of cores
    being used). These give explicit details about each step of the
    simulation. The MASTER is always labelled 0 (sortieproc.0) and all
    other cores are WORKERS.

3.  Output files where data for further analysis have a `.dat`
    extension:

    1.  `Energies.dat` lets you see the number of KMC step, CPU times,
        simulated times, activated barrier executed. It is contains the
        basic information about the evolution;

    2.  `Diffusion.dat` provides information on the mean squares
        displacement with the initial configuration as a reference;

    3.  `selec_ev.dat` provides information on the event chosen by the
        code at each KMC step.

    4.  `Gen_ev.dat` and `Spec_ev.dat` give respectively information on
        creation of generics and specifics events.

4.  Files `Topologies` and `topos.list` are a exhaustive list of all the
    the topology keys observed during the simulation.

5.  `allconf`s give us the possibility to visualize the spatial
    evolution atoms of the system during the
    simulation.(`allconf_defect` for example let us visualize only atoms
    in a noncristaline topology)

6.  Multiple `event_list_conf_x.dat` files provide a list of all the
    topologies and events present in configuration x.

7.  Finally, `events.uft` is an unformatted fortran file listing all the
    event of the simulation and the folder `EVENT_DIR` which describe a
    readable equivalent of `events.uft`

# Detailed content of output files

1.  Files *event_list_conf\_$n$* located in the *EVLIST_DIR* provides a
    exhaustive overview of the available transition of the simulation
    that leads to the KMC step $n$ for the local minimum $n-1$. It
    counts 11 columns

    (1) TypeId : Atomic type (be careful type does not mean chemical
        element i.e. Si1 Si2 are to different type of Si).

    (2) AtomId: Id of the Atom.

    (3) TopoId: Topology of the local initial minimum.

    (4) EventId : Generic Event Id.

    (5) Spec_id: Specific Event Id.

    (6) barrier: Refine or reconverge or clone energy barrier (in eV)
        see last logical column.

    (7) inv_bar: Refine or reconverge or clone energy of inverse barrier
        (in eV) see last logical column.

    (8) asy_ener: Difference between barrier and inv_barrier

    (9) inisad_dr: displacement between saddle and initial
        configuration.

    (10) inifin_dr: displacement between final and initial
         configuration.

    (11) noname logical: if true means the event was refined or
         reconverged otherwise it was cloned.


1.  The file *Energies.dat* provides a general overview of the evolution
    of the simulation. It counts 8 columns with each row providing the
    information for each minimum of energy $E_{Min}^n$ reached at step
    $n$):

    (1) Number of KMC step $n$.

    (2) CPU time.

    (3) Old minimum Energy $E_{Min}^{n-1}$.

    (4) New minimum energy $E_{Min}^n$.

    (5) Activation Energy or Energy barrier (or saddle energy minus the
        energy of the initial minimum) that is selected by kART from the
        list of all available events from this configuration, i.e.,
        $E^n_a=E^n_{Sad}-E_{Min}^{n-1}$.

    (6) Total rate sum of all the transition rates $\sum_i r_i$ that are
        extracted from the activation energy base on the transition
        state theory $r_i = \tau_0 e^{-E ^i_a/k_BT}$, at each step $n$.
        If a value of 0.0 is found, it means that the mean rate method
        (MRM) has been used for leaving a basin (in this case, the
        minimum energy shown in column 3 will not correspond to the
        value of column 4 at the previous step because internal dynamics
        in a basin is lost).

    (7) Time step (e.g., $\Delta t=-\ln \mu/ \sum_i r_i$, where $\mu$ is
        a random number in the interval $(0,1)$).

    (8) Simulated time (it is the accumulative sum of the previous time
        steps).

    (9) Selec Ev, id of the generic event selected for transition.

    (10) Init Topo, id of the initial topology.

    (11) Sad Topo, id of the saddle topology.

    (12) Fin Topo, id of the final topology.

2.  The file *Diffusion.dat* gives the information on the mean squares
    displacement with respect to the the initial configuration. It is
    displayed info on 4 columns:

    (1) Elapsed Time, time step $t_n$.

    (2) Sqr Displ, total square displacement,
        $\hbox{{\it SD}}(t_n)=\sum_{i=1}^N  \left(r_i(t_n)-r_i(0)\right)^2$,
        of $N$ atoms at $t_n$. If several atoms, the column is splitted
        into several subcolumns where the first is the Total *SD* of the
        system, the second is *SD* of the atoms of type 1, third is the
        *SD* of atoms of type 2, etc (the type order is the same as in
        the input file with the coord).

    (3) KMC step, is the step $n$.

    (4) Event delr, sum of the displacement from initial, $r^{ini}$, to
        final position, $r^{final}$, of the atoms that moved during the
        transition, that is, delr
        $=\sqrt{\sum_{i=1}^N  (r_i^{final}-r_i^{ini})^2}$. The sum
        usually corresponds to the displacement of the atom that moves
        most during the step; as the others remain at their initial
        positions (for example, if an interstitial, this value
        approximately corresponds to the displacement of the
        interstitial).

3.  The file *selec_ev.dat* gives us information on the event chosen by
    the code at each KMC step.\

    (1) CPU time: real time used to get that step.

    (2) selcBarrier: activation energy or energy barrier, that is
        $E^n_a=E^n_{Sad}-E_{Min}^{n-1}$.

    (3) KMC: number of KMC step $n$.

    (4) EVENT: the generic event selected during the step (same number
        found in file *Energies.dat* ).

    (5) SPEC_EVENT: the generic event executed during the step. For
        example, if the generic EVENT selected is 887554, there is a
        subset of specific events from which one is executed, for
        instance, the number 9 thus we identify it as 887554_9. In
        contrast to generic events, specific events take into account
        local deformations and stresses of the particular configuration
        at that KMC step (see section
        [9] sec:generating_events_art_part {reference-type="ref"
        reference="sec:generating_events_art_part"}).

    (6) SELEC_ATOM: main atom over the event is executed. It is the atom
        that advances most during the step.

    (7) timestep: as before $\Delta t=-\ln \mu/ \sum_i r_i$, where $\mu$
        is a random number in the interval $(0,1)$.

    (8) SimulT: (Simulated time), it is the accumulative sum of the
        previous time steps.

    (9) Basin ID: The basin ID indicates to thing if we are in a BMRM
        bassin (value $\ne0$) or not (value $0$). the non-value where we
        are in a BMRM bassin increment according to the number of
        different bassin visited. if this value as the same value for
        several following step this means that we are still in the same
        BMRM bassin.

    (10) Basin thresh: maximum height of barrier and inverse barrier for
         an event to be considered inside a basin used in the step as
         set by `MIN_SIG_BARRIER`.

4.  Files *Gen_ev.dat* give us information on creation of generics
    events. To avoid ambiguity, if one read the line at the $i^{th}$
    KMCstep this line represents the statistic about the transition from
    $i^{th}-1$ to $i^{th}$. It has the following information:

    (1) KMC: number of KMC step $n$.

    (2) Number of Generic event search: this is the total amount of ARTn
        search per KMC steps.

    (3) Number of saddle attempts: this is the total number of saddle
        attempts (for each ARTn search there is 10 independent attempts
        to find a saddle point) this column includes the failed and
        succeed attempts. Again per KMC steps.

    (4) Number of saddle found: the is the total number of saddle found
        per KMC steps.

    (5) Generic event found Excluding inverse event: count all
        successful generation of Generic events

    (6) Generic event found Including inverse event: count all
        successful generation of Generic events except the inverse
        Generic event.

    (7) New Generic event found Excluding inverse event: only count the
        one that are not in catalogue.

    (8) New Generic event found Including inverse event: only count the
        one that are not in catalogue.

    (9) Tot GenEv Incl InvEv: Total number of distinct Generic event in
        catalogue.

    (10) Force eval: this is the number of force evaluations for ARTn
         part of the code per KMC steps.

5.  The file *Spec_ev.dat* give us information on creation of specifics
    events.To avoid ambiguity, if one read the line at the $i^{th}$
    KMCstep this line represents the statistic about the transition from
    $(i-1)^{th}$ to $i^{th}$. It has the following information:

    (1) KMC: number of KMC step $n$.

    (2) \# GenEv to Consider: This is the number of Generic event to
        refine into Specific be careful this does not count the
        multiplicity of atoms sharing this Generic event.

    (3) \# of SpecEv to Refine: This is the number of Specific event to
        refine a saddle point contrary to the previous one this does
        count the multiplicity of atoms since it is a Spec event.

    (4) \# of SpecEv to Reconverge: This is the number of Specific event
        to reconverge a saddle point we do reconvergence of SpecEv when
        a previous SpecEv exists in the actual configuration.

    (5) \# of SpecEv to be Cloned: this is the number of event that
        should be cloned.

    (6) Spec Ev found: this is the number of Spec event found.

    (7) New Spec Ev: this is the number of new spec event found.

    (8) Tot SpecEv: this is the total number of distinct Specific event
        in catalogue.

    (9) Thres cloning: this is the energy barrier threshold at which we
        start cloning Generic events.

    (10) Ev Cloned: this is the number of cloned events

    (11) Tot Ev: this is the total number of available events that leads
         to the KMC step $n$.

6.  Files *Topologies* and *topos.list* are a exhaustive list of all the
    the topology keys observed during the simulation. Accordingly to the
    NAUTY the file *Topologies* basically shows:

    (1) topo_labels: topology label or hash key.

    (2) id1, id2, id3: the integers we use as input to a hash function
        to generate a unique topology number (hash key).

    (3) topo cutoff: it is the length-cutoff used by default to link two
        atoms (see input parameters for topology in KMC.sh file).

7.  The file *topo_stat.dat* has information about topologies at each
    step. It gives the following data:

    (1) CPU time.

    (2) currTOPO: is the current total number of topologies in the
        system. Equivalent to "known_tc\" variable in code and in
        sortiproc.0 file, it only increases if the variable `STATISTICS`
        is set to True.

    (3) activeTOPO: number of topologies that are active in the system
        (equivalent to variable "active_tc\" in sortiproc.0 file and in
        the code, it only increases if the variable `STATISTICS` is set
        to True).

    (4) newTOPO: the number of new topologies found at each step. The
        name of the variable in the code is "new_tc\" and only increases
        if the variable `STATISTICS` is set to True.

    (5) searchedTOPO: number of topologies total (all steps). The name
        of variable in the code is "searched_tc\" and only increases if
        the variable STATISTICS is set to True.

    (6) CumulTOPO: total number of topologies found in the simulation.
        The name of variable in the code is "cumul_tc\" and only
        increases if the variable `STATISTICS` is set to True.

    (7) diffTOPO: number of difficult topologies found during the step.

    (8) KMC: step.

    (9) SimulT: total time.

8.  *allconf* files give us the possibility to visualize the spatial
    evolution atoms of the system during the simulation. This file is
    given in XYZ format that can be visualized using for example, VMD,
    OVITO, VESTA, Rasmol, etc, programs. (*allconf_defect* for example
    let us visualize only atoms in a no-cristaline topology)

9.  Multiple *event_list_conf_n.dat* files are found in the dir
    EVLIST_DIR. Each file gives a list of the topologies and events
    found at each step that is used to create the catalog for choosing
    the next step (for example, *event_list_conf_8.dat* has the list
    used to choose the step 8, an other way to say it is that it has a
    list that leads from step 7 to 8). Each file gives the following
    data:

    (1) AtomId: id of the atom.

    (2) TopoId: id of corresponding topology of the AtomId.

    (3) EventId: id of one of the events associated to the the topology
        TopoId.

    (4) Spec_id: id for the specific event, which is used mostly for
        debugging or recovering detailed history of the kinetics. If
        Spec_id $= 1$ it is a generic event otherwise it is a specific
        event.

    (5) Barrier: barrier energy, $E_a=E_{sad}-E_{ini}$, corresponding to
        EventId.

    (6) Inv_bar: inverse barrier $E_i=E_{sad}-E_{final}$.

    (7) Asy_ener: final minus initial minimum energies:
        $E_{final}-E_{ini}$.

    (8) Inisad_dr: displacement from initial to saddle position of the
        atoms that moved most during the event finding, that is
        $\sqrt{\sum_{i=1}^N  (r_i^{sad}-r_i^{ini})^2}$.

    (9) Inifin_dr: same value as "Event delr\" in *Diffusion.dat* or
        displacement from initial to final position of the atoms that
        moved most during the event finding, that is
        $\sqrt{\sum_{i=1}^N  (r_i^{final}-r_i^{ini})^2}$.

10. File *events.uft*, is a unformatted fortran file listing all the
    events of the simulation contained in the folder *EVENT_DIR* (this
    folder contains the same information as *events.uft*, but human
    readable and given in different files). *events.uft* is useful for
    importing the list of events when we want to continue a previous
    simulation.

11. File *topos.list* has technical details of topologies of the system.
    Data is printed in two lines, the first has 6 fields: field one is
    the unique topology label or "hash key\" created from a set of three
    topology indexes given by NAUTY (fields from two to four); the field
    five is the `MAX_TOPO_CUTOFF` and the field six is a flag (`T` or
    `F`), for identifying a topology as a difficult or not. The second
    line has five fields: the first is
    this_topo%count,this_topo%attempts,this_topo%failed_attempts,
    this_topo%ncluster, this_topo%primary_topo_id [ \....STILL REMAINS
    TO EXPLAIN \....]{style="color: red"}

12. File *output.dat* prints a list of parameters and their setting
    values for the particular simulation. This file should be self
    explicative.

13. This file *KMC_log.txt* is a global resume of all the detail
    (physics parameters but also the version number of the code used
    even the date of the compilation) used in a simulation.

14. Finally, *sortieproc.n* files are the log files for each core
    running the simulation (labelled from 0 to N-1, where N is the
    number of cores being used). These give explicit details about each
    step of the simulation. The MASTER is always labelled 0
    (sortieproc.0) and all other cores are SLAVES.
