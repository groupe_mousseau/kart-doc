# Rerun a previous simulation

You can use results from a previous simulation in two ways: launching a
new simulation using an already computed catalogue or continuing a
previously stopped simulation.

## Using a previously computed catalog

It is always possible to continue a previous simulation once you have a
completed event catalogue. For this, you need two files:

`event.utf` and `topos.list`

It is also necessary to modify the following parameter in the
environment file.

        setenv  RESTART_IMPORT   .true.

## Continuing a previous simulation

To pursue a run, the following files are needed :

::: {.center}
  ---------------- -- --------------
   `initial.conf`      `topos.list`
    `this_conf`        `events.utf`
  ---------------- -- --------------
:::

Other files will be appended so they can be left in the directory. It is
suggested, to avoid problems, to simply copy all files into a new
directory and launch from it.

In any case, the following parameters also need to be set to true in the
environment file:

        setenv  RESTART_IMPORT   .true.
        setenv  RESTART_KMC      .true.
